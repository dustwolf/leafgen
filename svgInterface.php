<?php

class svgInterface {

 function __construct($height = 100, $width = 100) {
  header("Content-Type: image/svg+xml");
  ?><?xml version="1.0" encoding="UTF-8" standalone="no"?>
   <svg
    height="<?php echo $height; ?>" width="<?php echo $width; ?>"
    xmlns="http://www.w3.org/2000/svg"
   >
  <?php
 }
 
 public function background($color = "transparent") {
  ?><rect width="100%" height="100%" fill="<?php echo $color; ?>" /><?php
 }
 
 public function connectPoints($points, $color = "black", $width = "1px", $fill = "transparent") {
  foreach($points as $point) {
   $poly[] = $point["x"].",".$point["y"];
  }
  ?>
   <polyline points="<?php echo implode(" ", $poly); ?>"
             style="stroke:<?php echo $color; ?>;
                    stroke-width:<?php echo $width; ?>;
                    fill: <?php echo $fill; ?>; "/>
  <?php
 }
 
 public function path($coords, $color = "black", $width = "1px") {
  ?>
   <path 
    style="stroke:<?php echo $color; ?>;stroke-width:<?php echo $width; ?>"
    d="<?php echo $this->M($coords); ?>"
   />
  <?php
 }
 
 private function M($coords) {
  return "M ".$coords["x1"].",".$coords["y1"]." ".$coords["x2"].",".$coords["y2"];
 }
 
 function __destruct() {
  ?>
   </svg>
  <?php
 }

}
