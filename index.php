<?php

require_once "html.php";
$document = new html("Leaf generator", array(
 "bootstrap" => True,
 "css" => "style.css"
));

if($_SERVER['REQUEST_METHOD'] === 'POST') {
 require_once "hashInterface.php";
 $h = new hashInterface();

 $hash = $h->hashFile($_FILES["upload"]["tmp_name"]);
 ?>
 <img src="leaf.php?hash=<?php echo rawurlencode($hash); ?>"
      alt="leaf for your file" 
      style="height: 80%; 
             position: absolute; 
             top: 50%; 
             left: 50%; 
             transform: translate(-50%, -50%)" />
 <?php

} else {

 ?>
  <div class="text">
   <h1>Leaf Generator</h1>
   <p>Hello!</p>
   <p>This app generates a unique leaf from a file. Please provide a file:</p>
   <p>
    <form method="post" enctype="multipart/form-data">
     <input type="file" name="upload" id="upload">
     <input type="submit" value="Upload" name="submit">
    </form>
   </p>
   <p><a href="https://gitlab.com/dustwolf/leafgen">Source code</a>.</p>
   <p>This is a proof-of-concept application that does nothing but hashes the file and provides output in the form of a leaf.</p>
   <p>As humans we are wired to be able to easily distinguish leaves of different plants. Our brains find this information <a href="https://www.youtube.com/watch?v=-O5kNPlUV7w">beautiful</a>. Hashes on the other hand, not as much: d41d8cd98f00b204e9800998ecf8427e. Therefore this application encodes the hash as a leaf, making it much easier for us to recognize and distinguish (one file will always produce the same leaf if it contains the same content). Also, you get a pretty leaf!</p>
  </div>
 <?php
 
}
