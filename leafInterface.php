<?php

require_once "svgInterface.php";

class leafInterface {

 private $s;
 private $width;
 private $height;
 
 function __construct($width = 500, $height = 500, $background = "#43403c") {
  $this->width = $width;
  $this->height = $height;
  $this->s = new svgInterface($this->width, $this->height);
  $this->s->background($background);
 }
 
 private function offset($point, $offset = array("x" => 10, "y" => 10)) {
  return array(
   "x" => $point["x"] + $offset["x"],
   "y" => $point["y"] + $offset["y"]
  );
 }

 private function zoom($point, $zoom = array("x" => 10, "y" => 10)) {
  return array(
   "x" => $point["x"] * $zoom["x"],
   "y" => $point["y"] * $zoom["y"]
  );
 }
 
 private function radial($angle, $distance) { 
  $angle = deg2rad($angle);
  return array(
   "x" => $distance * cos($angle),
   "y" => $distance * sin($angle),
  );
 }

 public function generateLeaf($hash) {
  $leafArray = $this->hash2array($hash);
    
  $path = array();
  foreach($leafArray as $y => $x) {
  
   $position = $y / 31; //0-1
   $degrees = $position * 179 + 91;
   
   $oneSide = $this->width / 2;
   $distance = $x / 15; //x = 0-15
      

   $normalLeaf = (1 - sin(($position) * pi())) / 2 + 0.5;

   $distance = ($normalLeaf + $distance) / 2; //fit it to a normal leaf shape
   
   
   $point = $this->radial($degrees, $distance * $oneSide);
  
   $point = $this->offset($point, array("x" => $this->width / 2, "y" => $this->height / 2));
  
   $path[] = $point;
  }
  
  
  $path = $this->mirror($path);
  
  $this->s->connectPoints($path, "#afd100", "1px", "#008600");
 }
 
 private function mirror($path) {
  $both = array_reverse($path);
  foreach($path as $i => $point) {
   $both[] = array("x" => $this->width - $point["x"], "y" => $point["y"]);
  }
  return $both;
 }
 
 //split hash into array[0-31] = 0-15
 private function hash2array($hash) {
  $out = array();
  foreach(str_split($hash) as $letter) {
   $out[] = hexdec($letter);
  }
  return $out;
 }

}
