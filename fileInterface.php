<?php

class fileInterface {

 public $files;

 function __construct($path = False) {
  if($path === False) {
   $path = getcwd();
  }
  
  $this->files = glob($path."/*.txt");
 }
 
 function getFile($index) {
  if(isset($this->files[$index])) {
   return $this->files[$index];
  } else {
   return False;
  }
 }

}
